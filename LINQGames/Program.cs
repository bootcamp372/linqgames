﻿using System;

namespace LINQGames
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Supplier> suppliers = new List<Supplier> {
            new Supplier(101, "ACME", "acme.com"),
            new Supplier(201, "Spring Valley", "spring-valley.com"),
            };
            List<Product> products = new List<Product> {
            new Product(1, "Dark Chocolate Bar", 4.99M, 10, 101),
            new Product(2, "8 oz Guacamole", 5.99M, 27, 201),
            new Product(3, "Milk Chocolate Bar", 3.99M, 16, 101),
            new Product(4, "8 pkg Chicken Tacos", 15.99M, 7, 201),
            };


            int filterQuantity = 10;

            Console.WriteLine("Please select an option:");
            Console.WriteLine("1: Get All Supplier Names");
            Console.WriteLine("2: Filter By Price");
            Console.WriteLine("3: Filter By Quantity");
            Console.WriteLine("4: List all data about a supplier");
            Console.WriteLine("5: List first product that costs above a quantity");
            Console.WriteLine("6: Count products above a certain price");
            Console.WriteLine("7: Get items to be reordered");
            Console.WriteLine("Your choice:");

            int selection = Convert.ToInt32(Console.ReadLine());

            switch (selection)
            {
                case 1:
                    // write linq query to get names of all suppliers
                    GetAllNames(suppliers);
                    break;
                case 2:
                    // find products < 5.00
                    FilterByPrice(products);
                    break;
                case 3:
                    // find products of quantity > 10 and list in desc order
                    FilterByLowestQuantity(products);
                    break;
                case 4:
                    // list supplier data
                    ListSupplierData(suppliers);
                    break;
                case 5:
                    // list first product that costs above a quantity
                    ListFirstProduct(products);
                    break;
                case 6:
                    // list count of products above a quantity
                    CountProducts(products);
                    break;
                case 7:
                    // find items of certain quantity or less and convert to list
                    List<Product> productList = GetItemsToBeReordered(products);
                    if (productList.Count > 0)
                    {
                        foreach (var item in productList)
                        {
                            string productItem = productList.ToString();
                            Console.WriteLine(productItem);
                        }
                    }
                    break;
                default:
                    Console.WriteLine("Invalid Selection");
                    break;
            }




        }

        static void GetAllNames(List<Supplier> suppliers)
        {
            var query = from s in suppliers
                        select s.Name;
            Console.WriteLine("All Supplier Names");
            Console.WriteLine("-------------");
            Console.WriteLine();
            foreach (string name in query)
            {
                Console.WriteLine(name);
            }

        }

        static void FilterByPrice(List<Product> product)
        {
            Console.WriteLine("Enter highest price:");
            decimal filterPrice = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("All Product Items under {0:c}", filterPrice);
            Console.WriteLine("-------------");
            Console.WriteLine();
            var query = from p in product
                        where p.Price <= filterPrice
                        orderby p.Price ascending
                        select p;

            foreach (Product item in query)
            {
                string itemData = item.ToString();
                Console.WriteLine(itemData);
            }

        }

        static void FilterByLowestQuantity(List<Product> products)
        {
            Console.WriteLine("Enter lowest quantity:");
            int filterQuantity = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"All Items with Quantities of {filterQuantity} or more");
            Console.WriteLine("-------------");
            Console.WriteLine();
            var query = from p in products
                        where p.QuantityOnHand >= filterQuantity
                        orderby p.QuantityOnHand descending
                        select p;

            foreach (Product item in query)
            {
                Console.WriteLine(item.ProductName);
            }
            Console.WriteLine("-------------");
        }

        static void ListSupplierData(List<Supplier> suppliers)
        {

            Console.WriteLine("Enter supplier ID:");
            int supplierID = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"All Data for Supplier {supplierID}");
            Console.WriteLine("-------------");
            Console.WriteLine();

            Supplier supplier = (
                from s in suppliers
                where s.SupplierId == supplierID
                select s).FirstOrDefault();

            string supplierData = supplier.ToString();
            Console.WriteLine(supplierData);
        }

        static void ListFirstProduct(List<Product> products)
        {
            Console.WriteLine("Enter lowest price:");
            decimal filterPrice = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("First Product Item over {0:c}", filterPrice);
            Console.WriteLine("-------------");

            Product firstProduct = (
                from p in products
                where p.Price >= filterPrice
                select p).FirstOrDefault();

            string productData = firstProduct.ToString();
            Console.WriteLine(productData);
        }

        static void CountProducts(List<Product> products)
        {
            Console.WriteLine("Enter lowest price:");
            decimal filterPrice = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("-------------");
            Console.WriteLine();

            int count = (from p in products
                         where p.Price <= filterPrice
                         select p).Count();

            Console.WriteLine("There are {0} items under {1:C}", count, filterPrice);
        }

        static List<Product> GetItemsToBeReordered(List<Product> products)
        {
            Console.WriteLine("Enter highest quantity:");
            int filterQuantity = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------");
            Console.WriteLine();

            return (from p in products
                    where p.QuantityOnHand >= filterQuantity
                    select p).ToList();
        }
    }
}